package com.open.tvwidget.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.open.anim.AnimatorManager;
import com.open.anim.OpenAnim;
import com.open.anim.animator.ObjectNameAnimator;

/**
 * 自定义控件与绘制 + 属性动画 的结合
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 2019/3/17
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public class AnimView extends View {

    AnimatorManager mAnimatorManager;
    // 控件中心的X,Y坐标
    private int centerX;
    private int centerY;
    private float circleRadius = -1;
    // 内圆的画笔
    private Paint mCirclePaint;
    // 整个圆外切的矩形
    private RectF mRectF = new RectF();
    // 外层圆环的画笔
    private Paint mRingPaint;
    private int radius = 150;
    private float ringProgress = 0;

    public AnimView(Context context) {
        this(context, null);
    }

    public AnimView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnimView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
        initAnimator();
    }

    private void initPaint() {
        mRingPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRingPaint.setStyle(Paint.Style.STROKE);
        mRingPaint.setStrokeCap(Paint.Cap.ROUND);
        mRingPaint.setStrokeWidth(5);
        mRingPaint.setColor(Color.RED);
        //
        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCirclePaint.setColor(Color.BLUE);
    }

    private void initAnimator() {
        mAnimatorManager = OpenAnim.with(this)
                .into(this)
                .duration(1000)
                .startDelay(250)
                .interpolate(new LinearInterpolator())
                .repeatCount(ValueAnimator.INFINITE)
                // .repeatCount(3)
                .repeatMode(ValueAnimator.REVERSE)
//                .play(new ObjectNameAnimator("ringProgress", 0, 360))
//                .before(new ObjectNameAnimator("circleRadius", radius - 10, 0))
                .sequentially(
                        new ObjectNameAnimator("ringProgress", 0, 360),
                        new ObjectNameAnimator("circleRadius", radius - 10, 0)
                )
                .addListener(new AnimatorManager.OpenAnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }
                })
                .start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 画圆弧进度
        canvas.drawArc(mRectF, 90, ringProgress, false, mRingPaint);
        // 內圆绘制
//        if (ringProgress == 360) {
        canvas.drawCircle(centerX, centerY, circleRadius, mCirclePaint);
//        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        centerX = getMeasuredWidth() / 2;
        centerY = getMeasuredHeight() / 2;
        //设置圆圈的外切矩形
        mRectF.set(centerX - radius, centerY - radius, centerX + radius, centerY + radius);
    }

    @SuppressLint("ObjectAnimatorBinding")
    private float getCircleRadius() {
        return circleRadius;
    }

    @SuppressLint("ObjectAnimatorBinding")
    private void setCircleRadius(float circleRadius) {
        this.circleRadius = circleRadius;
        postInvalidate();
    }

    @SuppressLint("ObjectAnimatorBinding")
    private float getRingProgress() {
        return ringProgress;
    }

    @SuppressLint("ObjectAnimatorBinding")
    private void setRingProgress(float ringProgress) {
        this.ringProgress = ringProgress;
        postInvalidate();
    }

}
