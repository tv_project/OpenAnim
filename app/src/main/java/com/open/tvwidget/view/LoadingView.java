package com.open.tvwidget.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Size;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.open.anim.AnimatorManager;
import com.open.anim.OpenAnim;
import com.open.anim.animator.ObjectNameAnimator;
import com.open.anim.animator.WidthAnimator;
import com.open.tvwidget.R;

@SuppressLint("AppCompatCustomView")
public class LoadingView extends View {

    private static final int[] DEFAULT_LEVEL_COLORS = new int[]{Color.parseColor("#55ffffff"),
            Color.parseColor("#b1ff0000"), Color.parseColor("#ff0000ff")};

    private final Paint mPaint = new Paint();
    private final RectF mTempBounds = new RectF();
    private float mStrokeWidth = 10;
    private float mGroupRotation = 90f;
    @Size(3)
    private int[] mLevelColors = DEFAULT_LEVEL_COLORS;
    @Size(3)
    private float[] mLevelSwipeDegrees = new float[3];

    public LoadingView(Context context) {
        this(context, null);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeCap(Paint.Cap.ROUND);

        mLevelSwipeDegrees[0] = 30;
        mLevelSwipeDegrees[1] = 60;
        mLevelSwipeDegrees[2] = -90;

        OpenAnim.with(this)
                .into(this)
                .duration(3000)
                .repeatCount(ValueAnimator.INFINITE)
                .together(new ObjectNameAnimator("computeRender", 0.1f, 1.0f))
                .addListener(new AnimatorManager.OpenAnimatorListener() {
                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        mOriginEndDegrees = mEndDegrees;
                        mOriginStartDegrees = mEndDegrees;

                        mStartDegrees = mEndDegrees;
                        mRotationCount = (mRotationCount + 1) % (NUM_POINTS);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mRotationCount = 0;
                    }
                })
                .start();
    }

    private static final int DEGREE_360 = 360;

    private static final float MAX_SWIPE_DEGREES = 0.8f * DEGREE_360;
    private static final float[] LEVEL_SWEEP_ANGLE_OFFSETS = new float[]{1.0f, 7.0f / 8.0f, 5.0f / 8.0f};

    private static final float START_TRIM_DURATION_OFFSET = 0.5f;
    private static final float FULL_GROUP_ROTATION = 3.0f * DEGREE_360;
    private static final int NUM_POINTS = 5;


    private float mOriginStartDegrees;
    private float mOriginEndDegrees;

    private static final Interpolator LINEAR_INTERPOLATOR = new LinearInterpolator();

    @SuppressLint("ObjectAnimatorBinding")
    protected void setComputeRender(float renderProgress) {
        if (renderProgress <= START_TRIM_DURATION_OFFSET) {
            float startTrimProgress = (renderProgress) / START_TRIM_DURATION_OFFSET;
            mStartDegrees = mOriginStartDegrees + MAX_SWIPE_DEGREES * LINEAR_INTERPOLATOR.getInterpolation(startTrimProgress);
            float mSwipeDegrees = mEndDegrees - mStartDegrees;

            mLevelSwipeDegrees[0] = -mSwipeDegrees * LEVEL_SWEEP_ANGLE_OFFSETS[0] * (1.0f);
            mLevelSwipeDegrees[1] = -mSwipeDegrees * LEVEL_SWEEP_ANGLE_OFFSETS[1] * 1.0f;
            mLevelSwipeDegrees[2] = -mSwipeDegrees * LEVEL_SWEEP_ANGLE_OFFSETS[2] * (1.0f);
        }

        if (renderProgress > START_TRIM_DURATION_OFFSET) {
//            float endTrimProgress = (renderProgress - START_TRIM_DURATION_OFFSET) / (END_TRIM_DURATION_OFFSET - START_TRIM_DURATION_OFFSET);
//            mEndDegrees = mOriginEndDegrees + MAX_SWIPE_DEGREES * MATERIAL_INTERPOLATOR.getInterpolation(endTrimProgress);
        }

        mGroupRotation = ((FULL_GROUP_ROTATION / NUM_POINTS) * renderProgress) + (FULL_GROUP_ROTATION * (mRotationCount / NUM_POINTS));
        //
        postInvalidate();
    }

    private float mEndDegrees;
    private float mStartDegrees;
    private float mRotationCount;

    @SuppressLint("ObjectAnimatorBinding")
    private void setGroupRotation(float circleRadius) {
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int saveCount = canvas.save();

        mTempBounds.set(0, 0, getWidth(), getHeight());
//        mTempBounds.inset(mStrokeInset, mStrokeInset);
        canvas.rotate(mGroupRotation, getWidth() / 2, getHeight() / 2);

        for (int i = 0; i < 3; i++) {
//            if (mLevelSwipeDegrees[i] != 0) {
            mPaint.setColor(mLevelColors[i]);
            canvas.drawArc(mTempBounds, mEndDegrees, mLevelSwipeDegrees[i], false, mPaint);
//            }
        }

        canvas.restoreToCount(saveCount);
    }

    //    android:background="#fff1c02e"
//    app:loading_renderer="LevelLoadingRenderer"
}
