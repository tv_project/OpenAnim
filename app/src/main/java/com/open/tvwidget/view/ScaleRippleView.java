package com.open.tvwidget.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class ScaleRippleView extends View {

    public ScaleRippleView(Context context) {
        this(context, null);
    }

    public ScaleRippleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScaleRippleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    float mNewWidth = 0.0f;
    float mNewHeight = 0.0f;

    @SuppressLint("ObjectAnimatorBinding")
    private float getNewWidth() {
        return mNewWidth;
    }

    @SuppressLint("ObjectAnimatorBinding")
    private float getNewHeight() {
        return mNewHeight;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG|Paint.FILTER_BITMAP_FLAG));
        Paint paint = new Paint();
        RectF oval = new RectF();
        // paint.setAntiAlias(true);
        paint.setColor(Color.RED);
        paint.setStrokeWidth(1.5f);
        paint.setStyle(Paint.Style.STROKE); //空心的
        //椭圆左上角坐标（80.0f,120.0f），右上角坐标（400.0f,200.0f）
        oval.set(-getNewWidth()/2, -getNewHeight()/2, getWidth()/2 + getNewWidth()/2, getHeight()/6 + getNewHeight()/2);
        canvas.drawOval(oval, paint);
    }

    @SuppressLint("ObjectAnimatorBinding")
    private void setNewWidth(float width) {
        mNewWidth = width;
        postInvalidate();
    }

    @SuppressLint("ObjectAnimatorBinding")
    private void setNewHeight(float height) {
        mNewHeight = height;
        postInvalidate();
    }

}
