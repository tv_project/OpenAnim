package com.open.tvwidget.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class NewLineLayout extends LinearLayout {

    public NewLineLayout(Context context) {
        super(context);
    }

    public NewLineLayout(Context context,AttributeSet attrs) {
        super(context, attrs);
    }

    public NewLineLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setNewWidth(float width) {
        getLayoutParams().width = (int) width;
        requestLayout();
    }

}
