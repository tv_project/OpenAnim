package com.open.tvwidget;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.open.anim.OpenAnim;
import com.open.anim.animator.AddAnimator;
import com.open.anim.animator.AlphaAnimator;
import com.open.anim.animator.BaseAnimator;
import com.open.anim.animator.HeightAnimator;
import com.open.anim.animator.MoveXAnimator;
import com.open.anim.animator.MoveYAnimator;
import com.open.anim.animator.ObjectNameAnimator;
import com.open.anim.animator.RotationAnimator;
import com.open.anim.animator.TargetView;
import com.open.anim.animator.WidthAnimator;
import com.open.anim.evaluator.BounceEaseOut;

//public class AnimActivity extends FragmentActivity implements View.OnFocusChangeListener {
public class AnimActivity extends Activity implements View.OnFocusChangeListener {

    Button mTestBtn;
    Button mTestBtn2;
    ImageView mShouZhi;
    ImageView mDuiGou;
    View mScaleRiplleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTestBtn = findViewById(R.id.button);
        mTestBtn2 = findViewById(R.id.button2);
        mShouZhi = findViewById(R.id.showzhi_iv);
        mDuiGou = findViewById(R.id.duigoud_iv);
        mScaleRiplleView = findViewById(R.id.scale_ripple_view);
        mTestBtn.setOnFocusChangeListener(this);
        mTestBtn2.setOnFocusChangeListener(this);
        testAnim();
//        testAnim1();
//        testAnim2();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @SuppressLint("ResourceType")
    private void testAnim() {
        // OpenAnim.with(this)
        //         .into(mTestBtn)
        //         .together(
        //                 new TranslationXAnimator(500),
        //                 new TranslationYAnimator(100),
        //                 // new RotationXAnimator(0, 360),
        //                 // new RotationYAnimator(0, 360),
        //                 new RotationAnimator(0, 360))
        //         .duration(3000)
        //         // .interpolate(new LinearInterpolator())
        //         // .repeatCount(3)
        //         .repeatCount(ValueAnimator.INFINITE)
        //         .start();

        AddAnimator addAnimator = new AddAnimator();
        // addAnimator.setTypeEvaluator(new BounceEaseOut(500));
        addAnimator.playTogether(
                new ObjectNameAnimator(mScaleRiplleView, "newWidth", 0, 40, 0),
                new ObjectNameAnimator(mScaleRiplleView, "newHeight", 0, 10, 0));
        addAnimator.setTypeEvaluator(new BounceEaseOut(500));
        // addAnimator.setStartDelay(10000);
        addAnimator.setDuration(3000);

        RotationAnimator rotationAnimator = new RotationAnimator(mShouZhi, 0, 15, 0);
        // rotationAnimator.setTypeEvaluator(new BounceEaseOut(500));
        rotationAnimator.setDuration(1300);

        mShouZhi.setPivotX(300);
        mShouZhi.setPivotY(100);
        OpenAnim.with(this)
                // .together(R.anim.test_anim)
                .together(
                        new RotationAnimator(mShouZhi, 0, 15, 0).setDuration(1300),
                        new AddAnimator().playSequentially(
                                new ObjectNameAnimator(mScaleRiplleView, "newWidth", 0, 40, 0),
                                new ObjectNameAnimator(mScaleRiplleView, "newHeight", 0, 10, 0))
//                                .setTypeEvaluator(new BounceEaseOut(500))
                                .setDuration(3000)
                )
                // .play(addAnimator).with(rotationAnimator)
                // .duration(1500)
//                .interpolate(new AccelerateDecelerateInterpolator())
                .repeatCount(ValueAnimator.INFINITE)
//                 .typeEvaluator(new BounceEaseOut(500))
//                 .repeatCount(3)
                .startDelay(1000)
                .start();

        OpenAnim.with(this)
                .into(new TargetView(mTestBtn2))
                .together(
                        new MoveXAnimator(50, 200, 10),
                        new MoveYAnimator(50, 200, 10),
                        new WidthAnimator(50, 300, 10),
                        new HeightAnimator(50, 300, 10))
                .repeatCount(ValueAnimator.INFINITE)
//                .repeatCount(3)
                .duration(2000)
                .start();

        // Animator animator = AnimatorInflater.loadAnimator(this, R.anim.test_anim);
        // animator.setTarget(mTestBtn2);
        // animator.start();
        // if (animator instanceof AnimatorSet) {
        //     AnimatorSet animatorSet = (AnimatorSet) animator;
        //     Log.d("hailong.qiu", "getChildAnimations:" + animatorSet.getChildAnimations());
        // }
    }

    /**
     * together: 同时一起执行
     * sequentially: 顺序执行
     */
    private void testAnim1() {
        OpenAnim.with(this).together(
                new MoveXAnimator(mTestBtn2, 500),
                new MoveYAnimator(mTestBtn, 100))
                .duration(3000)
                .repeatCount(ValueAnimator.INFINITE)
                .start();
    }

    private void testAnim2() {
        // 按钮1的动画集合.
        BaseAnimator testAnimator = OpenAnim.with(this)
                .into(mTestBtn)
                .sequentially(new MoveXAnimator(), new AlphaAnimator())
                .getAnimator();
        // 按钮2的动画集合.
        BaseAnimator test2Animator = OpenAnim.with(this)
                .into(mTestBtn2)
                .sequentially(new MoveYAnimator(), new AlphaAnimator())
                .getAnimator();
        OpenAnim.with(this)
                // .sequentially(testAnimator, test2Animator)
                .together(testAnimator, test2Animator)
                .duration(3000)
                .repeatCount(3) // BUG:多个动画组合的时候，重复次数无效.
                .start();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        // AnimatorManager am = OpenAnim.with(this).into(v).together(
        //         new ScaleXAnimator(hasFocus ? 1.0f : 1.5f, hasFocus ? 1.5f : 1.0f),
        //         new ScaleYAnimator(hasFocus ? 1.0f : 1.5f, hasFocus ? 1.5f : 1.0f))
        //                              .duration(300)
        //                              .start();
        // am.onDestroy();
        // if (hasFocus) {
        //     OpenAnim.into(v).duration(1300).play(new MoveYAnimator(-30)).start();
        // } else {
        //     OpenAnim.into(v).duration(1300).play(new MoveYAnimator(0)).start();
        // }
        // OpenAnim.into(v).duration(1300).play(new MoveYAnimator(hasFocus ? -30 : 0)).start();
    }

}
