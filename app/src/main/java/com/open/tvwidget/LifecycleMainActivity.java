package com.open.tvwidget;

import android.app.Activity;
import android.app.Fragment;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.OnLifecycleEvent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class LifecycleMainActivity extends Activity implements LifecycleOwner {
// public class MainActivity extends FragmentActivity {

    private LifecycleRegistry mLifecycleRegistry;

    @Override
    protected void onStart() {
        super.onStart();
        mLifecycleRegistry.markState(Lifecycle.State.STARTED);
        dispatch(Lifecycle.Event.ON_START);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        mLifecycleRegistry = new LifecycleRegistry(this);
        NewLiveData liveData = new NewLiveData(this);
        // liveData.observe(this, new Observer<Integer>() {
        //     @Override
        //     public void onChanged(@Nullable Integer integer) {
        //         Log.d("hailong.qiu", "onChanged integer:" + integer);
        //     }
        // });
        mLifecycleRegistry.markState(Lifecycle.State.CREATED);
        dispatch(Lifecycle.Event.ON_CREATE);
    }

    /**
     * 发送刷新的信号.
     */
    private void dispatch(Lifecycle.Event event) {
        Activity activity = this;
        if (activity instanceof LifecycleRegistryOwner) {
            ((LifecycleRegistryOwner) activity).getLifecycle().handleLifecycleEvent(event);
            return;
        }

        if (activity instanceof LifecycleOwner) {
            Lifecycle lifecycle = ((LifecycleOwner) activity).getLifecycle();
            if (lifecycle instanceof LifecycleRegistry) {
                ((LifecycleRegistry) lifecycle).handleLifecycleEvent(event);
            }
        }
    }

    public class NewLiveData implements LifecycleObserver {

        private boolean isLive = false;

        public NewLiveData(LifecycleOwner owner) {
            // getLifecycle().addObserver((LifecycleObserver) LifecycleMainActivity.this.getLifecycle());
            // mLifecycleRegistry.addObserver(this);
            owner.getLifecycle().addObserver(this);
        }

        // @Override
        // public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<Integer> observer) {
        //     super.observe(owner, observer);
        //     // owner.getLifecycle().addObserver(owner.getLifecycle().);
        // }

        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        public void testOnCreate(){
            Log.e("hailong.qiu","testOnCreate");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void testOnStart(){
            Log.e("hailong.qiu","testOnStart");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        public void testOnPause(){
            Log.e("hailong.qiu","testOnPause");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        public void testOnStop(){
            Log.e("hailong.qiu","testOnStop");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void testOnDestroy(){
            Log.e("hailong.qiu","testOnDestroy");
        }

        // @Override
        // protected void onActive() {
        //     super.onActive();
        //     // 激活活动 onStart ... ...
        //     Log.d("hailong.qiu", "onActive");
        // }
        //
        // @Override
        // protected void onInactive() {
        //     super.onInactive();
        //     // 停止活动 onStop ... ...
        //     Log.d("hailong.qiu", "onInactive");
        // }

    }

}
