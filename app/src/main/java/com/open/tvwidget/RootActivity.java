package com.open.tvwidget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RootActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.anim_btn, R.id.recyclerview_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.anim_btn: // 动画Activity.
                startActivity(new Intent(this, AnimActivity.class));
                break;
            case R.id.recyclerview_btn: // RecyclerView Activity.
                startActivity(new Intent(this, RecyclerViewAcitivty.class));
                break;
        }
    }

}
