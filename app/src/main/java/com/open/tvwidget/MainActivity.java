package com.open.tvwidget;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.open.anim.OpenAnim;
import com.open.anim.animator.MoveYAnimator;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        OpenAnim.with(this)
                .into(findViewById(R.id.button))
                .together(
                new MoveYAnimator(500),
                new MoveYAnimator(100))
                .duration(3000)
                // .repeatCount(ValueAnimator.INFINITE)
                .repeatCount(3)
                .start();
    }

}
