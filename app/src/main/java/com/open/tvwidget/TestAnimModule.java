package com.open.tvwidget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

import com.open.anim.AnimConfigBuilder;
import com.open.anim.AnimModule;
import com.open.anim.animator.BaseAnimator;
import com.open.anim.evaluator.BounceEaseOut;

/**
 * 动画公共模块全局配置 ==> 测试模块
 */
public class TestAnimModule implements AnimModule {

    @Override
    public void applyOptions(@NonNull Context context, @NonNull AnimConfigBuilder builder) {
        // builder.interpolate(new DecelerateInterpolator())
        builder
                // .duration(1000)
                // .typeEvaluator(new BounceEaseOut(500))
                .interpolate(new LinearInterpolator());
    }

}
