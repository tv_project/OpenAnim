# 介绍

OpenAnim 是一个动画库，以简洁的方式使用动画



![anim1](https://gitee.com/kumei/OpenAnim/raw/master/doc/img/anim1.gif)



# Download

```java
// 必须使用
implementation 'com.open.tvwidget:anim:1.2.0'
// 依赖添加
implementation 'com.android.support:appcompat-v7:28.0.0' // 不使用 root.gradle
```


# TODO列表:
* 支持 XML动画(补齐  play ater befer with，sequentially 等)

- 酷炫复杂动画支持(doing) ~8.20 绘制酷炫的函数封装起来.(doing)
- RecyclerView Anim 支持(todo) SVG 动画支持???(todo)  litho 支持???(todo) 
- 支持全局的暂时/继续执行(滚动优化，参考 Glide.with(context).resumeRequests(); Glide.with(context).pauseRequests();) ，清除Clear. (doing) 9.9~
- TransitionDrawable 转场动画支持.(todo)
- RecyclerVeiw,Lenaback下的性能测试，内存是否泄漏(todo)



# V1.2.2

* 修复 生命周期BUG，只能两个生效
* 支持 width height 改变的动画



# v1.2.1([文档](https://gitee.com/hailongqiu/OpenAnim/raw/master/doc/AnimDoc_v1.2.1.zip))
* BUG：多个不同的时间无效的问题
* 支持 TypeEvaluator
* play ater befer with 支持设置属性
* 局部延时支持
* 支持XML动画



# v1.2.0
* 修改 包名问题



# v1.1.1([文档](https://gitee.com/hailongqiu/OpenAnim/raw/master/doc/AnimDoc_v1.1.1.zip))
* 修改支持生命周期, 添加with,into方法.
* 全局配置插值器,时间支持



# v1.0.2
* 修改支持多个view
* 修改支持多个view同时动画
* 支持生命周期
Activity或Fragment的onStart而resume，onStop而pause，onDestroy而clear，
还有 View 的 onAttachedToWindow，onDetachedFromWindow(clear)
从而节约流量和内存，并且防止内存泄露






# 中央仓库使用

自建库需要如何加，打开 build.gradle

```groovy
apply plugin: 'com.android.library'
apply plugin: 'maven' // 加入这句

android {
	... ...
}

dependencies {
	... ...
}

//上传到中央仓库.
def MAVEN_LOCAL_PATH = ''
def ARTIFACT_ID = 'anim' // 一般为别名
def VERSION_NAME = '1.0.0' // 版本号
def GROUP_ID = 'com.open.tvwidget' // 统一使用包名
def ACCOUNT = 'admin'
def PASSWORD = 'admin123'
// 最后会组装成这样 com.open.tvwidget:anim:1.0.0，这样调用 implementation 'com.open.tvwidget:anim:1.0.0'
//脚本：将Lib打成aar包上传至maven私有库
uploadArchives {
    repositories {
        mavenDeployer {
            repository(url: MAVEN_LOCAL_PATH) {
                authentication(userName: ACCOUNT, password: PASSWORD)
            }
            pom.project {
                groupId GROUP_ID
                artifactId ARTIFACT_ID
                version VERSION_NAME
                packaging 'aar'
            }
        }
    }
}
```



引用库的时候，在项目的 build.gradle 的 android->repositories 里面加入 maven

```groovy
android {
    ... ...
	repositories {
    	flatDir {
        	dirs 'libs'
    	}
    	maven {
        	url MAVEN_LOCAL_PATH
    	}
	}
}
```



