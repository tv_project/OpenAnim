package com.open.anim;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.animation.LinearInterpolator;

import com.open.anim.animator.BaseAnimator;

/**
 * 默认配置类
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 19-03-15
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public class BaseAnimModule implements AnimModule {

    @Override
    public void applyOptions(@NonNull Context context, @NonNull AnimConfigBuilder builder) {
        builder.interpolate(new LinearInterpolator());
               // .duration(BaseAnimator.DEFUALT_DURATION);
    }

}
