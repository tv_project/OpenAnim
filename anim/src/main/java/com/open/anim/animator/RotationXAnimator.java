package com.open.anim.animator;

import android.view.View;

/**
 * 绕着水平X中心线旋转
 */
public class RotationXAnimator extends ObjectNameAnimator {

    public RotationXAnimator(View view, float... items) {
        super(view, "rotationX", items);
    }

    public RotationXAnimator(float... items) {
        super("rotationX", items);
    }

}
