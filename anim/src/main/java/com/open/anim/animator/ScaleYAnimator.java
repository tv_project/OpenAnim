package com.open.anim.animator;

import android.view.View;

/**
 * 缩放动画效果
 */
public class ScaleYAnimator extends ObjectNameAnimator {

    public ScaleYAnimator() {
        this(0.0f, 1.0f);
    }

    public ScaleYAnimator(float... items) {
        super("scaleY", items);
    }

    public ScaleYAnimator(View view, float... items) {
        super(view, "scaleY", items);
    }

}
