package com.open.anim.animator;

import android.view.View;

public interface ITargetView {
    public View getView();
}
