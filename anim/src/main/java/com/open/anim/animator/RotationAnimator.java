package com.open.anim.animator;

import android.view.View;

/**
 * 绕着中心点平面旋转
 */
public class RotationAnimator extends ObjectNameAnimator {

    public RotationAnimator(View view, float... items) {
        super(view, "rotation", items);
    }

    public RotationAnimator(float... items) {
        super("rotation", items);
    }

}
