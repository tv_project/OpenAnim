package com.open.anim.animator;

import android.view.View;

/**
 * 绕着水平Y中心线旋转
 */
public class RotationYAnimator extends ObjectNameAnimator {

    public RotationYAnimator(View view, float... items) {
        super(view, "rotationY", items);
    }

    public RotationYAnimator(float... items) {
        super("rotationY", items);
    }

}