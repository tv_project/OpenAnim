package com.open.anim.animator;

import android.view.View;

/**
 * 高度动画
 */
public class HeightAnimator extends ObjectNameAnimator {

    public HeightAnimator(float... items) {
        super("height", items);
    }

    public HeightAnimator(View view, float... items) {
        super(view, "height", items);
    }

}
