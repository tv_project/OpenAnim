package com.open.anim.animator;

import android.view.View;

/**
 * 移动动画效果
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 19-03-15
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public class MoveXAnimator extends ObjectNameAnimator {

    public MoveXAnimator() {
        this(0, 25, -25, 25, -25, 15, -15, 6, -6, 0);
    }

    public MoveXAnimator(View view, float... items) {
        super(view, "translationX", items);
    }

    public MoveXAnimator(float... items) {
        super("translationX", items);
    }

}
