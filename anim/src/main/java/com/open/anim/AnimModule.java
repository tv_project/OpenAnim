package com.open.anim;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * 全局模块配置接口
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 19-03-15
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public interface AnimModule {

    void applyOptions(@NonNull Context context, @NonNull AnimConfigBuilder builder);

}
