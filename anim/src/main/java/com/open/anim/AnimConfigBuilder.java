package com.open.anim;

import android.animation.TypeEvaluator;
import android.content.Context;
import android.view.animation.Interpolator;

/**
 * 全部配置的 Builder
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 19-03-15
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public class AnimConfigBuilder {

    private Interpolator mInterpolator = null;
    private long mDuration = -1;
    private TypeEvaluator mTypeEvaluator = null;

    public AnimConfigBuilder interpolate(Interpolator interpolator) {
        mInterpolator = interpolator;
        return this;
    }

    /**
     * 设置动画时间
     */
    public AnimConfigBuilder duration(long duration) {
        mDuration = duration;
        return this;
    }

    public AnimConfigBuilder typeEvaluator(TypeEvaluator typeEvaluator) {
        mTypeEvaluator = typeEvaluator;
        return this;
    }

    OpenAnim build(Context context) {
        return new OpenAnim(context, mInterpolator, mDuration, mTypeEvaluator);
    }

}



